package id.ac.ui.tebakorgan;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import static id.ac.ui.tebakorgan.Contract.ALL_SOALS;
import static id.ac.ui.tebakorgan.Contract.CONTENT_PATH;
import static id.ac.ui.tebakorgan.Contract.CONTENT_URI;
import static id.ac.ui.tebakorgan.Contract.ALL_SOALS;
import static id.ac.ui.tebakorgan.Contract.AUTHORITY;
import static id.ac.ui.tebakorgan.Contract.CONTENT_PATH;
import static id.ac.ui.tebakorgan.Contract.MULTIPLE_RECORDS_MIME_TYPE;
import static id.ac.ui.tebakorgan.Contract.SINGLE_RECORD_MIME_TYPE;
import static java.lang.Integer.parseInt;

/**
 * Created by Lathifah Alfat on 11/15/2017.
 */

public class SoalProvider extends ContentProvider {

    private static final String TAG = SoalProvider.class.getSimpleName();

    private static UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private DBHelper mDB;

    private static final int URI_ALL_SOALS = 10;
    private static final int URI_SINGLE_SOAL = 20;

    @Override
    public boolean onCreate() {
        mDB = new DBHelper(getContext());
        initializeUriMatching();
        return true;
    }

    private void initializeUriMatching(){
        sUriMatcher.addURI(AUTHORITY, CONTENT_PATH, URI_ALL_SOALS);
        sUriMatcher.addURI(AUTHORITY, CONTENT_PATH+"/#", URI_SINGLE_SOAL);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor cursor = null;
        switch (sUriMatcher.match(uri)) {
            case URI_ALL_SOALS:
                cursor = mDB.query(ALL_SOALS);
                break;
            case URI_SINGLE_SOAL:
                cursor = mDB.query(parseInt(uri.getLastPathSegment()));
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case ALL_SOALS:
                return MULTIPLE_RECORDS_MIME_TYPE;
            case URI_SINGLE_SOAL:
                return SINGLE_RECORD_MIME_TYPE;
            default:
                return null;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id = mDB.insert(values);
        return Uri.parse(CONTENT_URI + "/" + id);
    }
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return mDB.delete(parseInt(selectionArgs[0]));
    }
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return mDB.update(parseInt(selectionArgs[0]), values.getAsString("word"));
    }
}
