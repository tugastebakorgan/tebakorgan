package id.ac.ui.tebakorgan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CategoryAdapter ca;
    private ArrayList<Category> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        images = listImage();

        ca = new CategoryAdapter(images, this);
        recyclerView.setAdapter(ca);

        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(lm);
    }

    public ArrayList<Category> listImage() {
        ArrayList<Category> images = new ArrayList<>();
        images.add(new Category(R.drawable.jantung_kategori));
        images.add(new Category(R.drawable.kategori_pernapasan));
        images.add(new Category(R.drawable.ekskresi));
        images.add(new Category(R.drawable.pencernaan_kategori));
        return images;
    }

    class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

        private final LayoutInflater mInflater;
        private Context mContext;
        private ArrayList<Category> images;

        public CategoryAdapter(ArrayList<Category> images, Context c) {
            this.images = images;
            mContext = c;
            mInflater = LayoutInflater.from(c);
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView iv;
            public ViewHolder(View view) {
                super(view);
                mContext = view.getContext();
                iv = (ImageView)view.findViewById(R.id.gambar_jantung);
//                view.setClickable(true);
//                view.setOnClickListener(this);
            }
//            public void onClick(View v){
//                mContext.startActivity(new Intent(mContext, NumberActivity.class));
//            }
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.jantung, parent, false);
            return new ViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.iv.setImageResource(images.get(position).imageId);
            holder.iv.setOnClickListener(new View.OnClickListener(){
                int pos = position+1;
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(mContext, NumberActivity.class);
                    //intent.putExtra();
                    startActivity(intent);
                    Toast.makeText(CategoryActivity.this, "buka number activity "+pos, Toast.LENGTH_SHORT).show();
                }
            });
        }
        @Override
        public int getItemCount() {
            return images.size();
        }


    }
}
