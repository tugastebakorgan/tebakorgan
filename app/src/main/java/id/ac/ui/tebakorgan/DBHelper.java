package id.ac.ui.tebakorgan;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.content.ContentValues.TAG;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_GAMBAR;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_HINT;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_ID;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_JAWABAN;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_KATEGORI;
import static id.ac.ui.tebakorgan.Contract.SoalList.KEY_SOAL;
import static id.ac.ui.tebakorgan.Contract.SoalList.TABLE_SOAL;
import static id.ac.ui.tebakorgan.Contract.ALL_SOALS;
import static id.ac.ui.tebakorgan.Contract.DATABASE_NAME;


/**
 * Created by Lathifah Alfat on 10/30/2017.
 */

public class DBHelper extends SQLiteOpenHelper{

    private static final String TABLE_CREATE =
            "CREATE TABLE if not exists "+TABLE_SOAL+" ("+
                    KEY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                    KEY_KATEGORI +" INTEGER, "+
                    KEY_SOAL +" INTEGER, "+
                    KEY_GAMBAR +" STRING, "+
                    KEY_HINT +" TEXT, "+
                    KEY_JAWABAN +" STRING));";

    SQLiteDatabase mWritableDB;
    SQLiteDatabase mReadableDB;


    public DBHelper (Context context){
        super(context,DATABASE_NAME,null,1);
    }

    @Override
    public void onCreate (SQLiteDatabase db){
        db.execSQL(TABLE_CREATE);
        fillDatabaseWithData(db);
    }

    public void fillDatabaseWithData(SQLiteDatabase db){
        String[][][] soals = {
                {
                        {"1","1","R.drawable.aorta", "A _ _ R _ _","aorta"},
                        {"1","2","R.drawable.bilikkanan","B _ L _ K  K _ N A _","bilik kanan"},
                        {"1","3","R.drawable.bilikkiri","B _ L _ K  K _ R _","bilik kiri"},
                        {"1","4","R.drawable.serambikanan","S _ R _ M _ I  K _ N A _","serambi kanan"},
                        {"1","5","R.drawable.serambikiri","S _ R _ M _ I  K _ R _","serambi kiri"},
                        {"1","6","R.drawable.vena","V _ _ A","vena"},

                        {"2","1","R.drawable.alveolus", "A _ V E _ _ U _","alveolus"},
                        {"2","2","R.drawable.bronkeolus","B _ O _ K E _ L _ _","bronkeolus"},
                        {"2","3","R.drawable.bronkus","B _ _ N _ S ","bronkus"},
                        {"2","4","R.drawable.diafragma","D _ A _ R _ G M _","diafragma"},
                        {"2","5","R.drawable.faring","F _ R I _ _ ","faring"},
                        {"2","6","R.drawable.laring","L _ R _ N _","laring"},

                        {"3","1","R.drawable.uretra","U _ E T _ A","uretra"},
                        {"3","2","R.drawable.ginjal","G _ N J _ L","ginjal"},
                        {"3","3","R.drawable.kandungkemih","K _ N D _ _ G  K _ M I _","kandung kemih"},
                        {"3","4","R.drawable.kelenjarkeringat","K _ L E _ J _ R  K _ R I _ _ A T","keringat"},
                        {"3","5","R.drawable.kelenjarsebaceous","K _ L E _ J _ R S _ B A _ E _ U S","kelenjar sebaceous"},
                        {"3","6","R.drawable.salurankencing","S _ L U _ _ N  K _ _ C I _ G","saluran kencing"},

                        {"4","1","R.drawable.anus", "A _ U _","anus"},
                        {"4","2","R.drawable.duodenum","D _ O D _ _ U M","duodenum"},
                        {"4","3","R.drawable.empedu","E _ P _ D _","empedu"},
                        {"4","4","R.drawable.esofagus","E _ _ F _ G U _","esofagus"},
                        {"4","5","R.drawable.kerongkongan","K _ R O _ _ K _ N _ A N","kerongkongan"},
                        {"4","6","R.drawable.lambung", "L _ M B _ N _","lambung"},}

        };
        ContentValues values = new ContentValues();
        for(int i =0; i<soals.length; i++){
            for(int y=0; y<soals[i].length; y++){
                for(int x=0; x<soals[i][y].length; x++)
                values.put(KEY_ID, soals[i][y][x]);
                db.insert(TABLE_SOAL, null, values);
            }
        }

    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS"+TABLE_SOAL);
        onCreate(db);
    }

    public Cursor query(int position){
        String query;
        if (position != ALL_SOALS){
            position++;
            query = "SELECT * FROM "+TABLE_SOAL+" WHERE "+KEY_ID+" = "+position+" ;";
        } else{
            query = "SELECT * FROM "+TABLE_SOAL+" ;";
        }

        Cursor cursor = null;
        try{
            if(mReadableDB == null){
                mReadableDB = this.getReadableDatabase();
            }

            cursor = mReadableDB.rawQuery(query, null);
        }catch (Exception e){

        }finally {
            return cursor;
        }
    }

    public long insert(ContentValues values){
        long added = 0;
        try {
            if (mWritableDB == null) {
                mWritableDB = getWritableDatabase();
            }
            added = mWritableDB.insert(TABLE_SOAL, null, values);
        } catch (Exception e) {
            Log.d(TAG, "INSERT EXCEPTION " + e);
        }
        return added;
    }

    public int update(int id, String soal) {
        int updated = 0;
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_SOAL, soal);
            if (mWritableDB == null) {
                mWritableDB = getWritableDatabase();
            }
            updated = mWritableDB.update(TABLE_SOAL, //table to change
                    values, // new values to insert
                    KEY_ID + " = ?", // selection criteria for row (in this case, the _id column)
                    new String[]{String.valueOf(id)}); //selection args; the actual value of the id

        } catch (Exception e) {
            Log.d(TAG, "UPDATE EXCEPTION " + e);
        }
        return updated;
    }
    public int delete(int id) {
        int deleted = 0;
        try {
            if (mWritableDB == null) {
                mWritableDB = this.getWritableDatabase();
            }
            deleted = mWritableDB.delete(TABLE_SOAL,
                    KEY_ID + " = ? ", new String[]{String.valueOf(id)});
        } catch (Exception e) {
            Log.d (TAG, "DELETE EXCEPTION " + e);
        }
        return deleted;
    }
}
