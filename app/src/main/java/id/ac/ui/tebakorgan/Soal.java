package id.ac.ui.tebakorgan;



/**
 * Created by Lathifah Alfat on 11/21/2017.
 */

public class Soal {

    public Soal() {
    }
    public int kategori;
    public int ID_soal;
    public String gambar_soal;
    public String hint;
    public String jawaban;

    public int getKategori() {
        return kategori;
    }

    public void setKategori(int kategori) {
        this.kategori = kategori;
    }

    public int getID_soal() {
        return ID_soal;
    }

    public void setID_soal(int ID_soal) {
        this.ID_soal = ID_soal;
    }

    public String getGambar_soal() {
        return gambar_soal;
    }

    public void setGambar_soal(String gambar_soal) {
        this.gambar_soal = gambar_soal;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

}


