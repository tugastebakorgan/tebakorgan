package id.ac.ui.tebakorgan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import static android.R.attr.gravity;
import static android.view.Gravity.CENTER;

public class NumberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);

        GridView gridview = (GridView) findViewById(R.id.gridNumber);
        gridview.setAdapter(new NumberAdapter(this));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                Intent intent = new Intent(NumberActivity.this, PlayActivity.class);
                startActivity(intent);
            }
        });
    }
}

class NumberAdapter extends BaseAdapter{
    Context mContext;

    //inisialisasi variabel via array
    int id_soal[]={
            1,2,3,4,5,6
    };

    public NumberAdapter(Context c){
        mContext = c;
    }

    @Override
    public int getCount() {
        //jumlah array
        return id_soal.length;
    }

    @Override
    public Object getItem(int position) {
        //posisi pada array
        return id_soal[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(mContext);
        textView.setText(String.valueOf(id_soal[position]));
        textView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        textView.setTextColor(Color.parseColor("#000000"));
        textView.setTextSize(36);
        textView.setGravity(CENTER);
        textView.setPadding(5,10,5,10);
        return textView;
    }
}