package id.ac.ui.tebakorgan;

import static id.ac.ui.tebakorgan.R.id.imageView;

/**
 * Created by Lathifah Alfat on 11/19/2017.
 */

public class Category {
    public int imageId;

    Category(int imageId){
        this.imageId = imageId;
    }
}
