package id.ac.ui.tebakorgan;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void menuCategory(View view){
        Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
        startActivity(intent);
    }
    public void menuHow(View view){
        Intent intent = new Intent(MainActivity.this, HowActivity.class);
        startActivity(intent);
    }
    public void menuScore(View view){
        Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
        startActivity(intent);
    }
    public void menuAbout(View view){
        Intent intent = new Intent(MainActivity.this, AboutActivity.class);
        startActivity(intent);
    }
    public void menuExit(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Anda yakin ingin keluar dari permainan ini?")
                .setNegativeButton("Ya",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick (DialogInterface dialog, int which){
                        MainActivity.this.finish();
                    }
                })
                .setPositiveButton("Tidak", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick (DialogInterface dialog, int which){
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
