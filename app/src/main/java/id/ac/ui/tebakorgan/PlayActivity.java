package id.ac.ui.tebakorgan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PlayActivity extends AppCompatActivity {

    private EditText editText;
    private Button buttonCek;
    private String jawaban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        editText = (EditText)findViewById(R.id.editText);
        buttonCek = (Button) findViewById(R.id.buttonCek);

        buttonCek.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                jawaban = editText.getText().toString();
                if(jawaban.equalsIgnoreCase("aorta")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(PlayActivity.this);
                    builder.setMessage("Jawaban Anda Benar!")
                            .setNeutralButton("OK",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which){
                                    // Do call some activity. Do what you    wish to;
                                    startActivity(new Intent(PlayActivity.this,NumberActivity.class));
                                }
                    })
                            .show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(PlayActivity.this);
                    builder.setMessage("Jawaban Anda Salah!")
                            .setNeutralButton("Kembali",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick (DialogInterface dialog, int which){
                                    //call class NumberActivity;
                                    startActivity(new Intent(PlayActivity.this,NumberActivity.class));
                                }
                            })
                            .setNegativeButton("Coba Lagi",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick (DialogInterface dialog, int which){
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
    }
}
