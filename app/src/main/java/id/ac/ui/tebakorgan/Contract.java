package id.ac.ui.tebakorgan;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Lathifah Alfat on 11/22/2017.
 */

public class Contract {
    private static final String TAG = Soal.class.getSimpleName();

    public static final int ALL_SOALS = -2;
    public static final String AUTHORITY = "id.ac.ui.tebakorgan.provider";

    public static final String CONTENT_PATH = "soal";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + CONTENT_PATH);

    static final String SINGLE_RECORD_MIME_TYPE =
            "vnd.android.cursor.item/vnd.id.ac.ui.tebakorgan.provider.soal";
    static final String MULTIPLE_RECORDS_MIME_TYPE =
            "vnd.android.cursor.item/vnd.id.ac.ui.tebakorgan.provider.soal";
    public static final String DATABASE_NAME = "TebakOrgan.db";

    public static abstract class SoalList implements BaseColumns {
        public static final String TABLE_SOAL = "soal";

        public static final String KEY_ID = "ID";
        public static final String KEY_KATEGORI = "kategori";
        public static final String KEY_SOAL = "ID_soal";
        public static final String KEY_GAMBAR = "gambar_soal";
        public static final String KEY_HINT = "hint";
        public static final String KEY_JAWABAN = "jawaban";

    }
}
